cmake_minimum_required(VERSION 3.17)

set(CMAKE_TOOLCHAIN_FILE toolchain.cmake)

set(CMAKE_MAKE_PROGRAM "C:/Program Files/mingw-w64/x86_64-8.1.0-posix-seh-rt_v6-rev0/mingw64/bin/mingw32-make.exe" CACHE FILEPATH "" FORCE)
set(CMAKE_MAKE_PROGRAM "C:/Program Files (x86)/mingw-w64/i686-8.1.0-posix-dwarf-rt_v6-rev0/mingw32/bin/mingw32-make.exe" CACHE FILEPATH "" FORCE)
#set(CMAKE_MAKE_PROGRAM "C:/Tools/ON Semiconductor/RSL10_IDE_3_3/IDE_V3.2.2.13/arm_tools/bin/make.exe" CACHE FILEPATH "" FORCE)
set(CMAKE_MAKE_PROGRAM make CACHE FILEPATH "" FORCE)

project(cmake_driven_example_arm-none-eabi-gcc C)

set(SOURCES
	main.c
	modules/math/math.c
)

include_directories(
	modules/math
)

set(CMAKE_C_FLAGS "-DINCLUDE_MATH")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DMYVAR=1")

# Invoking: Cross ARM C Linker
set(CMAKE_EXE_LINKER_FLAGS "-Xlinker --gc-sections")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -T\"${CMAKE_SOURCE_DIR}/sdk_packages/rsl10_sdk/source/firmware/cmsis/source/GCC/sections_modified.ld\"")


# build elf file
add_executable(${PROJECT_NAME} ${SOURCES})
target_link_libraries(${PROJECT_NAME}
                        ${CMAKE_SOURCE_DIR}/sdk_packages/rsl10_sdk/lib/ble_core/Release/libblelib.a
                        ${CMAKE_SOURCE_DIR}/sdk_packages/rsl10_sdk/lib/ble_core/Release/libkelib.a
)