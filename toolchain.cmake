set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)
set(CMAKE_CROSSCOMPILING 1)

set(CMAKE_TRY_COMPILE_TARGET_TYPE "STATIC_LIBRARY")
set(CMAKE_C_OBJCOPY arm-none-eabi-objcopy)

if(1)
	set(CMAKE_C_COMPILER "C:/Users/cwalch1/Desktop/gcc-arm-none-eabi-9-2020-q2-update-win32/bin/arm-none-eabi-gcc.exe")
	set(CMAKE_C_COMPILER "C:/Users/cwalch1/Desktop/gcc-arm-none-eabi-8-2019-q3-update-win32/bin/arm-none-eabi-gcc.exe")
	set(CMAKE_C_COMPILER arm-none-eabi-gcc)
	# not working: spaces or dots in path 
	#set(CMAKE_C_COMPILER "C:/Tools/ON Semiconductor/RSL10_IDE_3_3/IDE_V3.2.2.13/arm_tools/bin/arm-none-eabi-gcc.exe")

else()
	set(CMAKE_C_COMPILER gcc)
endif()